FRC Team 4272 SafetyApp
=======================

The app currently supports Android, and running as a Web application.
iOS support is written, but Apple's App-Store approval takes time and
money.

-   [Download for Android](../releases/SafetyApp-latest.apk) — Requires
    enabling installing apps from unknown sources; since I'm not sucking
    up to Google to get it in the Play store. To do this, go to Settings
    → Applications, then check "Unknown Sources"
-   [Web application](./web/) — Should work pretty much anywhere. Taking
    photos when reporting an injury may or may not work, depending on
    your browser (verified to work in Firefox on Desktop and Android).

The layout of the Android application will be extra-ugly on old versions
of Android, but if you have a version of android from the last 3 years,
you should be in good shape. If you do happen to be locked to an old
version of Android, you may want to try installing a recent version of
Firefox Mobile, and using the web app.

Also, the [source code](https://git.team4272.com/2017/safetyapp.git)
because [Free Software](https://www.gnu.org/philosophy/free-sw) legal
and ethical reasons.
