---
title:
---

Platinum tier:

- [Caterpillar](http://www.cat.com/en_US.html)
- [Purdue FIRST Programs](http://www.purduefirst.org/)

Gold tier:

- [Schilli](http://schilli.com/)
- [Southwire](http://www.southwire.com/)
- E4 Performance
- [Indiana Space Grant Consortium (INSGC)](https://engineering.purdue.edu/INSGC)

Silver tier:

- [Wabash National](http://www.wabashnational.com/)
- [TBIRD](http://tbirddesign.com/)
- [Cotton Threads Shirt Company](https://www.etsy.com/shop/CottonThreadsShirtCO)
- [Lafayette Quality Products Inc.](http://www.lqp-mfg.com/)
- [The Kelly Group](http://www.thekelly-group.com/)
- [Small Parts, Inc.](http://www.smallpartsinc.com/)
- [Ivy Tech](https://www.ivytech.edu/)
- [Matrix Technologies Incorporated](http://matrixti.com/)

Bronze tier:

- [MacAllister Machinery](https://www.macallister.com/)
- Fishers and Associates
- [Wampler's Disposal Center](http://wamplerservicesinc.com/)
- [Steiner Enterprises, Inc.](http://www.steineronline.com/)
- [M.P. Baker Electric, Inc.](http://mpbaker.com/)
- Nufer Designs
- Lafayette Auto Supply
- [Meyer Plastics, Inc.](http://www.meyerplastics.com/)
- The Tulpin Family
- [Lafayette Community Bank](https://www.lafayettecommunitybank.com)
- [Scott Greeson](http://scottgreesonmusic.com/)
- [Casey's General Store](https://www.caseys.com/)
- NoteTech Industries
- [KIL Architecture and Planning](http://www.kilarchitecture.com/)
- [Lafayette Tool and Die](http://www.lafayettetoolanddie.com/)
- [John Vanderkolk Construction, Inc.](https://www.vanderkolkconstruction.com/)
- Gibson's Shaved Ice

and a Special Thanks to [McCutcheon High School](http://mhs.tsc.k12.in.us/).
