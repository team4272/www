<div>

### [Sponsors](/sponsors.html)

Platinum tier

- [Caterpillar](http://www.cat.com/en_US.html)
- [Purdue FIRST Programs](http://www.purduefirst.org/)

Gold Tier

- [Schilli](http://schilli.com/)
- [Southwire](http://www.southwire.com/)
- E4 Performance
- [Indiana Space Grant Consortium (INSGC)](https://engineering.purdue.edu/INSGC)

</div>

<div>

### [Sponsors (cont.)](/sponsors.html)

Silver tier

- [Wabash National](http://www.wabashnational.com/)
- [TBIRD](http://tbirddesign.com/)
- [Cotton Threads Shirt Company](https://www.etsy.com/shop/CottonThreadsShirtCO)
- [Lafayette Quality Products Inc.](http://www.lqp-mfg.com/)
- [The Kelly Group](http://www.thekelly-group.com/)
- [Small Parts, Inc.](http://www.smallpartsinc.com/)
- [Ivy Tech](https://www.ivytech.edu/)
- [Matrix Technologies Incorporated](http://matrixti.com/)

Bronze tier

- [see all sponsors...](/sponsors.html)

</div>

<div>

### About

- FRC team 4272 is based out of
  [McCutcheon High School](http://mhs.tsc.k12.in.us/) in Lafayette,
  IN.
- [Website Source Code](https://git.team4272.com/www.git)

</div>
