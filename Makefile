# Set the default target #######################################################
phony += all
all:

# Boilerplate ##################################################################
SHELL = bash
phony += FORCE
.DELETE_ON_ERROR:
.SECONDARY:

# Development server ###########################################################
serve: serve-8000
serve-%: all
	dir=$$(mktemp -d -t) && trap 'fusermount -u "$$dir"; rmdir "$$dir"' EXIT && unionfs -o ro out:src "$$dir" && (cd "$$dir" && python3 -m http.server $*)
phony += serve serve-%

# Real rules to actually make the website ######################################

RUBYLIB=$(realpath .)/lib
export RUBYLIB

patsubst-all = $(if $1,$(call patsubst-all,$(wordlist 2,$(words $1),$1),$2,$(patsubst $(firstword $1),$2,$3)),$3)

html.suffixes = md org erb
html.src = $(shell find src -type f \( -false $(foreach s,$(html.suffixes), -o -name '*.$s' -o -name '*.$s.m4' ) \) | grep -v /web/)
html.out = $(call patsubst-all,$(addprefix src/%.,$(html.suffixes)),out/%.html,$(patsubst %.m4,%,$(html.src)))
all: $(html.out)
all: out/style.css
all: out/header-path.svg

pagerender.deps = bin/pagerender lib/pandoc.rb lib/template.erb lib/footer.md

out/%: src/%.m4
	mkdir -p -- $(@D)
	m4 -P < $< > $@
out/%: out/%.m4
	m4 -P < $< > $@

out/%.html: src/%.md $(pagerender.deps)
	mkdir -p -- $(@D)
	bin/pagerender $< > $@
out/%.html: out/%.md $(pagerender.deps)
	bin/pagerender $< > $@

out/%.html: src/%.org $(pagerender.deps)
	mkdir -p -- $(@D)
	bin/pagerender $< > $@
out/%.html: out/%.org $(pagerender.deps)
	bin/pagerender $< > $@

out/%.html: src/%.erb $(pagerender.deps)
	mkdir -p -- $(@D)
	bin/pagerender $< > $@
out/%.html: out/%.erb $(pagerender.deps)
	bin/pagerender $< > $@

var-%:
	@printf '%s\n' $(call quote.shell,$($*))

out/%.css: src/%.scss
	scss -s < $< > $@

# Recurse into the logos folder ################################################
outdir=out/logos
srcdir=src/logos
topoutdir=out
topsrcdir=src
include src/logos/Makefile

# Boilerplate ##################################################################

.PHONY: $(phony)

define nl


endef

# I put this as the last line in the file because it confuses Emacs syntax
# highlighting and makes the remainder of the file difficult to edit.
quote.shell = $(subst $(nl),'$$'\n'','$(subst ','\'',$1)')
